import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImoveisNovosComponent } from './imoveis-novos.component';

describe('ImoveisNovosComponent', () => {
  let component: ImoveisNovosComponent;
  let fixture: ComponentFixture<ImoveisNovosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImoveisNovosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImoveisNovosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
