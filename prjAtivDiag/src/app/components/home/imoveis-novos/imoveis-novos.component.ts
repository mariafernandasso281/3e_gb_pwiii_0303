import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-imoveis-novos',
  templateUrl: './imoveis-novos.component.html',
  styleUrls: ['./imoveis-novos.component.css']
})
export class ImoveisNovosComponent implements OnInit {

  public card = [
    {
      mostrar:"active",
      cards: [
        {
          id : 0,
          img : 'https://resizedimgs.zapimoveis.com.br/fit-in/800x360/vr.images.sp/44e13b051e3fa0186fc28e3d3d718b97.jpg',
          title: 'Apartamento - Itajaí, Bairro Itaipava',
          desc: 'Apartamento lindo para locação, possui 2 quartos. 40m2 de pura beleza. ',
          category: ['fitness', 'piscina', 'novo'],

          price: 'R$ 1,400'
        },
        {
          id : 1,
          img : 'https://resizedimgs.zapimoveis.com.br/fit-in/800x360/vr.images.sp/0c2124f4dc44b178f8e1f2959d60bd0e.jpg',
          title: 'Apartamento - Jacarepaguá, Rio de Janeiro',
          desc: 'Apartamento lindo para locação, possui 2 quartos. Além disso, possui uma piscina muito grande e bonita.',
          category: ['festa', 'legal','novo'],

          price: 'R$ 1,800'
        },
        {
          id : 2,
          img : 'https://resizedimgs.zapimoveis.com.br/fit-in/800x360/vr.images.sp/b19b87bd61d8461889611ac3d2aa61c5.jpg',
          title: 'Apartamento - São paulo, 2 quartos, 30 m2',
          desc: 'Apartamento lindo para locação, possui 2 quartos. Além disso, possui uma piscina muito grande e bonita.',
          category: ['playground', 'aceita crianças','novo'],

          price: 'R$ 1,250'
        }
      ]
    },
    {
      mostrar: '',
      cards: 
      [
        {
          id : 3,
          img : 'https://imgbr.imovelwebcdn.com/avisos/resize/2/29/56/18/12/30/1200x1200/2248915721.jpg',
          title: 'Apartamento - Consolação, São Paulo',
          desc: 'Apartamento lindo para locação, possui 2 quartos. Além disso, possui uma piscina muito grande e bonita.',
        category: ['fitness', 'piscina','novo'],
          price: 'R$ 1,870'
        },
        {
          id : 4,
          img : 'https://imgbr.imovelwebcdn.com/avisos/resize/2/29/58/47/79/41/1200x1200/2352832542.jpg',
          title: 'Apartamento - Inajau, Bairro Soquina',
          desc: 'Apartamento lindo para locação, possui 2 quartos. Além disso, possui uma piscina muito grande e bonita.',
          category: ['fitness', 'piscina','novo'],
          price: 'R$ 1,940'
        },
        {
          id : 5,
          img : 'https://imgbr.imovelwebcdn.com/avisos/resize/2/29/64/15/63/67/1200x1200/2599749735.jpg',
          title: 'Casa - Ludimeiros, Santa Catarina',
          desc: 'Excelente residência, voltada para o verde, repleta de armários, com 4 suítes sendo 1 uma no térreo e uma master, salas de estar com lareira, TV e jantar, escritório, lavabo, cozinha, despensa, lavanderia, dependência de empregados, área gourmet com churrasqueira e forno de pizza, piscina e garagem com 8 vagas sendo 3 cobertas.',
          category: ['garagem', 'piscina','lavanderia'],
          price: 'R$ 1,900'
        }
      ]
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
