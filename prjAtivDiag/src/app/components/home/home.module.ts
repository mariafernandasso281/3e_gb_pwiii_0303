import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { DestaquesComponent } from './destaques/destaques.component';
import { FiltroComponent } from './filtro/filtro.component';
import { ImoveisNovosComponent } from './imoveis-novos/imoveis-novos.component';



@NgModule({
  declarations: [
    HomeComponent,
    DestaquesComponent,
    FiltroComponent,
    ImoveisNovosComponent
  ],
  imports: [
    CommonModule
  ]
})
export class HomeModule { }
