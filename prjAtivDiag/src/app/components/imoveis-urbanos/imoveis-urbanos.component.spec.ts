import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImoveisUrbanosComponent } from './imoveis-urbanos.component';

describe('ImoveisUrbanosComponent', () => {
  let component: ImoveisUrbanosComponent;
  let fixture: ComponentFixture<ImoveisUrbanosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImoveisUrbanosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImoveisUrbanosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
